const express = require('express'),
    bodyparser = require('body-parser'),
    Mongoose = require('mongoose'),
    routes = require('./src/routes/routes'),
    cors = require('cors'),
    app = express(),
    port = 3333 || Process.env.PORT;

Mongoose.connect('mongodb+srv://insta:insta@cluster0-0ntne.mongodb.net/cnc?retryWrites=true&w=majority', {
    useNewUrlParser: true,
    useUnifiedTopology: true
})

app.use(cors());
app.use(bodyparser.json());
app.use(bodyparser.urlencoded({ extended: true }))
app.use(routes);

app.listen(port, () => console.log('conectado na porta', port))