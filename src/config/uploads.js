const multer = require('multer'),
path = require('path');

module.exports = {
    storage: multer.diskStorage({
        destination: path.resolve(__dirname,'..','..','uploads'),
        filename: (req, file, callback) => {
            callback(null, `${file.originalname.slice(0,-4)}-${Date.now()}${path.extname(file.originalname)}`)
        } 
    })
}