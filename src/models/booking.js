const mongoose = require('mongoose');

const bookingSchema = new mongoose.Schema({
    date: String,
    aproved: Boolean,
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
    place:{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Place' 
    }
})

module.exports = mongoose.model('booking', bookingSchema);