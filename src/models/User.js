const mongoose = require('mongoose'),
  userSchema = new mongoose.Schema({
    email: String,
  },
    {
      timestamps: true,
    }
  );

module.exports = mongoose.model('User', userSchema);