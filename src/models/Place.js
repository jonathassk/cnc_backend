const mongoose = require('mongoose'),
  placeSchema = mongoose.Schema({
    title: String,
    description: String,
    price: Number,
    address: String,
    techs: [String],
    images: String,
    user: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'user',
    }
  },
    {
      timestamps: true,
    }
  )

module.exports = mongoose.model('place', placeSchema);

