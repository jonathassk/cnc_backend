const express = require('express'),
    multer = require('multer'),
    uploadConfig = require('../config/uploads'),
    placeController = require('../controller/placecontroller'),
    userController = require('../controller/usercontroller'),
    dashboardController = require('../controller/dashboardcontroller'),
    bookingController = require('../controller/bookingcontroller'),
    routes = express.Router(),
    upload = multer(uploadConfig);

//place routes    
routes.post('/place', upload.single('images'), placeController.store)
routes.get('/place', placeController.index)

//users routes
routes.post('/newuser', userController.store)

//dashboard routes
routes.get('/dashboard', dashboardController.show)

//booking routes
routes.post('/place/:place_id/booking', bookingController.store)

module.exports = routes;