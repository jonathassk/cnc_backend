const Place = require('../models/Place');

module.exports = {
    async show(req, res){
        const {user_id} = req.headers;
        places = await Place.find({ user: user_id })
        return res.json(places);
    }
 }