const Booking = require('../models/booking')

module.exports = {
    async store(req, res) {
        const { user_id } = req.headers,
            { place_id } = req.params,
            { date } = req.body;

        const booking = await Booking.create({
            user: user_id,
            place: place_id,
            date
        })

        await booking.populate('user').execPopulate();

        return res.json(booking);
    }
}