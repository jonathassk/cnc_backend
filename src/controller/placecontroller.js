const Place = require('../models/Place'),
    User = require('../models/User'),
    multer = require('multer');

module.exports = {
    async index(req, res) {
        const { techs } = req.query;
        const places = await Place.find({ techs })
        return res.json(places);
    },
    async store(req, res) {
        const { title, description, price, address, techs } = req.body,
            { filename } = await req.file,
            { user_id } = req.headers;
            const user = await User.findById(user_id);
        
        if(!user){
            return res.status(400).json({ error: 'user does not exist' })
        }
    
        const place = await Place.create({
            user: user_id,
            title,
            description,
            price,
            address,
            images: filename,
            techs: techs.split(',')
                .map(tech => tech.trim()),
        })

        return res.json(place)
    },
    async show(req, res) {

    }
}